const Path                          = require('path');
const Webpack                       = require('webpack');
const {CleanWebpackPlugin}          = require('clean-webpack-plugin');
const MiniCssExtractPlugin          = require('mini-css-extract-plugin');
const DuplicatePackageCheckerPlugin = require('duplicate-package-checker-webpack-plugin');
const ManifestPlugin                = require('webpack-manifest-plugin');
const TerserJSPlugin                = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin       = require('optimize-css-assets-webpack-plugin');

require('dotenv').config({path: Path.resolve(`${__dirname}/.env`)});

let is_production = null;

process.argv.forEach((argument) => {
    if (/^--env=/.test(argument)) {
        let match     = argument.match(/^--env=(.+?)$/);
        is_production = (match[1] === 'production');
    }
});

if (is_production === null) {
    is_production = (process.env.APP_ENV === 'production');
}

const dev_server_domain = process.env.WEBPACK_DOMAIN;
const dev_server_port   = process.env.WEBPACK_PORT;
const dev_server_https  = process.env.WEBPACK_HTTPS !== 'false';

let hmr,
    source_maps;

if (process.env.WEBPACK_DISABLE_HMR !== undefined) {
    hmr = process.env.WEBPACK_DISABLE_HMR !== 'true';
} else {
    hmr = false;
}

if (process.env.WEBPACK_ENABLE_SOURCE_MAPS !== undefined) {
    source_maps = process.env.WEBPACK_ENABLE_SOURCE_MAPS !== 'false';
} else {
    source_maps = false;
}

if (is_production) {
    hmr         = false;
    source_maps = false;
}

module.exports = {
    mode: is_production ? 'production' : 'development',
    optimization: {
        usedExports: true,
        minimizer: [
            new TerserJSPlugin({
                parallel: true,
                extractComments: true,
                terserOptions: {
                    keep_fnames: true,
                    keep_classnames: true,
                    mangle: true,
                },
            }),
            new OptimizeCSSAssetsPlugin({
                cssProcessorOptions: {
                    safe: true,
                    discardComments: {
                        removeAll: true,
                    },
                },
            }),
        ],
    },
    externals: {
        window: 'window',
        document: 'document',
    },
    entry: {
        app_css: Path.resolve(`${__dirname}/_resources/assets/css/app.scss`),
        app_js: Path.resolve(`${__dirname}/_resources/assets/js/app.js`),
    },
    output: {
        path: Path.resolve(`${__dirname}/public/build`),
        publicPath: is_production ? '/build/' : (dev_server_https ? 'https' : 'http') + `://${dev_server_domain}:${dev_server_port}/build/`,
        filename: 'js/' + (is_production ? '[chunkhash]' : '[name].[hash]') + '.js',
    },
    resolve: {
        alias: {
            node_modules: Path.resolve(`${__dirname}/node_modules`),
            magnificPopup: Path.resolve(`${__dirname}/node_modules/magnific-popup/dist/jquery.magnific-popup.js`),
            
            css: Path.resolve(`${__dirname}/_resources/assets/css`),
            js: Path.resolve(`${__dirname}/_resources/assets/js`),
            img: Path.resolve(`${__dirname}/_resources/assets/images`),
            fnt: Path.resolve(`${__dirname}/_resources/assets/fonts`),
            modules: Path.resolve(`${__dirname}/_resources/modules`),
            
        },
        modules: [
            Path.resolve(`${__dirname}/node_modules`),
        ],
    },
    module: {
        rules: [
            {
                test: /\.(sa|sc|c)ss$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            hmr: hmr,
                        },
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: source_maps,
                            importLoaders: 1,
                        },
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            sourceMap: source_maps,
                        },
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: source_maps,
                        },
                    },
                ],
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
            },
            // Fonts
            {
                test: /\.(woff(2)?|eot|ttf|otf)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[hash].[ext]',
                            outputPath: 'f',
                        },
                    },
                ],
            },
            // Images
            {
                test: /\.(ico|gif|png|jp(e)?g|svg)$/i,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[hash].[ext]',
                            outputPath: 'i',
                        },
                    },
                ],
            },
        ],
    },
    plugins: [
        new ManifestPlugin({
            writeToFileEmit: true,
        }),
        new CleanWebpackPlugin(),
        new MiniCssExtractPlugin({
            filename: ! is_production ? 'css/[name].css' : 'css/[contenthash].css',
        }),
        new Webpack.DefinePlugin({
            __DEV__: ! is_production,
        }),
        new DuplicatePackageCheckerPlugin(),
        new Webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
        }),
    ],
    devtool: is_production ? false : (source_maps ? 'source-map' : 'eval'),
    devServer: {
        disableHostCheck: true,
        hot: true,
        contentBase: '/',
        proxy: {
            '*': (dev_server_https ? 'https' : 'http') + `://${dev_server_domain}:${dev_server_port}`,
        },
        https: dev_server_https,
        host: dev_server_domain,
        port: dev_server_port,
        headers: {
            'Access-Control-Allow-Origin': '*',
        },
        watchOptions: {
            aggregateTimeout: 300,
            ignored: /node_modules/,
            poll: 1000,
        },
    },
};
