<?php foreach ($menu as $menu_item): ?>
    <li>
        <?php if (!empty($menu_item['link'])): ?>
        <a href="<?= $menu_item['link'] ?>" class="<?= $class ?? '' ?>">
            <?php elseif ( ! empty($menu_item['scroll'])): ?>
                    <a href="javascript:void(0);" class="<?= $class ?? '' ?>"
                       data-js-scroll-to="<?= $menu_item['link'] ?>">
            <?php else: ?>
                    <a href="javascript:void(0);" class="disabled <?= $class ?? '' ?>">
            <?php endif; ?>
                <span><?= $menu_item["name"] ?></span>
                </a>
            </a>
        </a>
    </li>
<?php endforeach; ?>
