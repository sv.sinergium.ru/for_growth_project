<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"
    >
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.png" />
    
    <title><?= $title ?? "" ?></title>
    
    <meta property="og:title" content="<?= $title ?? "" ?>" />
    <meta property="og:image" content="<?= $og_image ?? "" ?>" />
    <meta property="og:description" content="<?= $description ?? "" ?>" />
    <meta name="description" content="<?= $description ?? "" ?>">
    
    <meta name="ym_id" content="<?= config('common.yandex_metrika_id') ?>">
    <meta name="g-key" content="<?= config('common.re_captcha.key') ?>">
    
    <link rel="stylesheet" href="<?= $css ?? "" ?>" />
</head>
<body>
<div class="content <?= is_dev() ? 'dev' : '' ?>">