<div class="wrapper">
    <section class="m-diagnostics-steps">
        <div class="row">
            <h2 class="title">
                Давайте представим, что вы записались <br> на диагностику - <b>что будет дальше?</b>
            </h2>
            <div class="m-diagnostics-steps__list">
                <div class="m-diagnostics-steps__items">
                    <span>Какая-то аннотация</span>
                    <div class="m-diagnostics-steps__item steps-item">
                        <div class="steps-item__content">
                            <h3 class="steps-item__title">
                                Познакомимся, бесплатно сделаем КТ, <br> проведём осмотр и составим план лечения
                            </h3>
                            <div class="steps-item__text">
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit perspiciatis odio enim.
                            </div>
                            <div class="steps-item__undertext undertext">
                                <div class="undertext__image">
                                    <img src="<?= img("img/img_1.png", "m-diagnostics-steps") ?>" data-js="lazy" alt="">
                                </div>
                                <div class="undertext__text">
                                    Lorem, ipsum dolor sit amet consectetur adipisicing elit - <span>1 900 P</span>
                                </div>
                            </div>
                        </div>
                        <figure class="steps-item__image">
                            <img src="<?= img("big/big_1.png", "m-diagnostics-steps") ?>" data-js="lazy" alt="">
                        </figure>
                    </div>
        
                    <div class="m-diagnostics-steps__item steps-item">
                        <div class="steps-item__content">
                            <h3 class="steps-item__title">
                                Заключим честный договор и зафиксируем цену, которая не изменится в процессе
                            </h3>
                            <div class="steps-item__text">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius maiores molestiae neque officiis pariatur perspiciatis quos.
                            </div>
                            <div class="steps-item__undertext undertext">
                                <div class="undertext__image">
                                    <img src="<?= img("logos/logo_1.png", "m-diagnostics-steps") ?>" data-js="lazy" alt="">
                                    <img src="<?= img("logos/logo_2.png", "m-diagnostics-steps") ?>" data-js="lazy" alt="">
                                    <img src="<?= img("logos/logo_3.png", "m-diagnostics-steps") ?>" data-js="lazy" alt="">
                                    <img src="<?= img("logos/logo_4.png", "m-diagnostics-steps") ?>" data-js="lazy" alt="">
                                </div>
                            </div>
                        </div>
                        <figure class="steps-item__image">
                            <img src="<?= img("big/big_2.png", "m-diagnostics-steps") ?>" data-js="lazy" alt="">
                        </figure>
                    </div>
                </div>
                
                <div class="m-diagnostics-steps__items">
                    <span>Какая-то аннотация</span>
                    <div class="m-diagnostics-steps__item steps-item">
                        <div class="steps-item__content">
                            <h3 class="steps-item__title">
                                Изготовим коронку или протез по вашим слепкам в собственной цифровой лаборатории
                            </h3>
                            <div class="steps-item__text">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum, excepturi, similique.
                            </div>
                            <div class="steps-item__undertext undertext">
                                <div class="undertext__image">
                                    <img src="<?= img("img/img_2.png", "m-diagnostics-steps") ?>" data-js="lazy" alt="">
                                </div>
                                <div class="undertext__text">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque doloribus eum impedit, itaque perspiciatis qui repudiandae!
                                </div>
                            </div>
                        </div>
                        <figure class="steps-item__image">
                            <img src="<?= img("big/big_3.png", "m-diagnostics-steps") ?>" data-js="lazy" alt="">
                        </figure>
                    </div>
    
                    <div class="m-diagnostics-steps__item steps-item">
                        <div class="steps-item__content">
                            <h3 class="steps-item__title">
                                Установим имплант без боли и стресса через микронадрез под анестезией или во сне
                            </h3>
                            <div class="steps-item__text">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum, excepturi, similique.
                            </div>
                            <div class="steps-item__undertext undertext">
                                <div class="undertext__image">
                                    <img src="<?= img("img/img_3.png", "m-diagnostics-steps") ?>" data-js="lazy" alt="">
                                </div>
                                <div class="undertext__text">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque doloribus eum impedit, itaque perspiciatis qui repudiandae!
                                </div>
                            </div>
                        </div>
                        <figure class="steps-item__image">
                            <img src="<?= img("big/big_4.png", "m-diagnostics-steps") ?>" data-js="lazy" alt="">
                            <div class="steps-item__image_small">
                                <img src="<?= img("small/small_1.png", "m-diagnostics-steps") ?>" data-js="lazy" alt="">
                            </div>
                        </figure>
                    </div>
                </div>
    
                <div class="m-diagnostics-steps__items">
                    <span>Какая-то аннотация</span>
                    <div class="m-diagnostics-steps__item steps-item">
                        <div class="steps-item__content">
                            <h3 class="steps-item__title">
                                Сразу же после импланта поставим коронку
                                или протез со сроком службы 20 лет
                            </h3>
                            <div class="steps-item__text">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum, excepturi, similique.
                            </div>
                            <div class="steps-item__undertext undertext">
                                <div class="undertext__image">
                                    <img src="<?= img("implants/implant_1.png", "m-diagnostics-steps") ?>" data-js="lazy" alt="">
                                    <img src="<?= img("implants/implant_2.png", "m-diagnostics-steps") ?>" data-js="lazy" alt="">
                                    <img src="<?= img("implants/implant_3.png", "m-diagnostics-steps") ?>" data-js="lazy" alt="">
                                </div>
                            </div>
                        </div>
                        <figure class="steps-item__image">
                            <img src="<?= img("big/big_5.png", "m-diagnostics-steps") ?>" data-js="lazy" alt="">
                            <div class="steps-item__image_small">
                                <img src="<?= img("small/small_2.png", "m-diagnostics-steps") ?>" data-js="lazy" alt="">
                            </div>
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>