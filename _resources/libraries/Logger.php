<?php

/**
 * Class Logger
 */
class Logger
{
    /**
     * @var string|null
     */
    private ?string $dir;
    
    /**
     * @var string|null
     */
    private ?string $file;
    
    /**
     * @var string|null
     */
    private ?string $data;
    
    /**
     * Logger constructor.
     *
     * @param string $name
     * @param array  $data
     */
    public function __construct(string $name, array $data)
    {
        $this->dir = ROOT_DIR . '/log/';
        
        if ( ! is_dir($this->dir)) {
            mkdir($this->dir, 0755);
        }
        
        $this->file = $this->dir . $name . '.log';
        
        if ( ! is_file($this->file)) {
            touch($this->file);
        }
        
        $this->data = json_encode($data);
    }
    
    /**
     * Сохранение файла лога
     *
     * @return bool
     */
    public function save()
    {
        try {
            $file_array = file($this->file);
            $file_lines = count($file_array);
            
            if ($file_lines >= 50) {
                array_splice($file_array, 0, 24);
                $handle      = fopen($this->file, 'w');
                $file_string = '';
                
                foreach ($file_array as $item) {
                    $file_string .= $item;
                }
                
                fwrite($handle, print_r($file_string, true));
                fclose($handle);
            }
            
            $handle = fopen($this->file, 'a+');
            fwrite($handle, "{$this->data}\n");
            fwrite($handle, "-----\n");
            fclose($handle);
            
            return true;
        } catch (Exception $e) {
            $subject = 'Ошибка записи лога ' . config('common.mail.logger.site_url') ?? get_array_value($_SERVER, "HTTP_HOST");
            send_mail('synergy.it@yandex.ru', $subject, $e->getMessage());
        }
        
        return false;
    }
}