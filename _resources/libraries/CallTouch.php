<?php

/**
 * Class CallTouch
 */
class CallTouch
{
    /**
     * Нода API
     *
     * @var mixed|null
     */
    protected $node = null;
    
    /**
     * ID сайта в КТ
     *
     * @var mixed|null
     */
    protected $site_id = null;
    
    /**
     * Телефон
     *
     * @var string|null
     */
    protected ?string $phone = null;
    
    /**
     * ID сессии КТ
     *
     * @var mixed|null
     */
    protected $session_id = null;
    
    /**
     * URL API
     *
     * @var string|null
     */
    protected ?string $url = null;
    
    /**
     * ФИо клиента
     *
     * @var string|null
     */
    protected ?string $fio = null;
    
    /**
     * Комментарий к заявке
     *
     * @var string|null
     */
    protected ?string $comment = null;
    
    /**
     * Страница отправки заявки
     *
     * @var string|null
     */
    protected ?string $page = null;
    
    /**
     * Название формы
     *
     * @var string|null
     */
    protected ?string $subject = null;
    
    /**
     * Тэги
     *
     * @var array|null
     */
    protected ?array $tags = null;
    
    /**
     * CallTouch constructor.
     *
     * @param string|int $phone Номер телефона
     * @param string|int|null $session_id ID сессии КТ (window.call_value)
     */
    public function __construct($phone, $session_id = null)
    {
        $this->node    = config('common.call_touch.node');
        $this->site_id = config('common.call_touch.site_id');
        
        if (empty($this->node)) {
            $this->url = "https://api.calltouch.ru/calls-service/RestAPI/requests/{$this->site_id}/register/";
        } else {
            $this->url = "https://api-node{$this->node}.calltouch.ru/calls-service/RestAPI/requests/{$this->site_id}/register/";
        }
        
        $this->phone      = preg_replace('/[^0-9]/', '', trim($phone));
        $this->session_id = trim($session_id);
    }
    
    /**
     * Добавляет ФИО
     *
     * @param string $fio
     *
     * @return $this
     */
    public function setFio(string $fio)
    {
        $this->fio = trim($fio);
        
        return $this;
    }
    
    /**
     * Добавляет комментарий
     *
     * @param string $comment
     *
     * @return $this
     */
    public function setComment(string $comment)
    {
        $this->comment = trim($comment);
        
        return $this;
    }
    
    /**
     * Добавляет страницу
     *
     * @param string $page
     *
     * @return $this
     */
    public function setPage(string $page)
    {
        $this->page = substr(strtok(trim($page), '?'), 0, 999);
        
        return $this;
    }
    
    /**
     * Добавляет имя формы
     *
     * @param string $subject
     *
     * @return $this
     */
    public function setSubject(string $subject)
    {
        $this->subject = trim($subject);
        
        return $this;
    }
    
    /**
     * Добавляет тэги
     *
     * @param array $tags
     *
     * @return $this
     */
    public function setTags(array $tags)
    {
        $this->tags = count($tags) ? implode(',', $tags) : null;
        
        return $this;
    }
    
    /**
     * Отправка запроса
     *
     * @return bool
     */
    public function createRequest()
    {
        if ( ! $this->site_id || ! $this->phone) {
            return false;
        }
        
        $query = [
            'phoneNumber' => $this->phone,
        ];
        
        if ($this->session_id) {
            $query['sessionId'] = $this->session_id;
        }
        
        if ($this->fio) {
            $query['fio'] = $this->fio;
        }
        
        if ($this->comment) {
            $query['comment'] = $this->comment;
        }
        
        if ($this->page) {
            $query['requestUrl'] = $this->page;
        }
        
        if ($this->subject) {
            $query['subject'] = $this->subject;
        }
        
        if ($this->tags) {
            $query['tags'] = $this->tags;
        }
        
        return $this->sendToCallTouch($query);
    }
    
    /**
     * Отправка данных
     *
     * @param array $query Параметры
     *
     * @return bool|string
     */
    private function sendToCallTouch(array $query)
    {
        if ( ! $this->site_id) {
            return false;
        }
        
        $channel = curl_init();
        
        curl_setopt($channel, CURLOPT_URL, $this->url . '?' . http_build_query($query));
        curl_setopt($channel, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($channel, CURLOPT_RETURNTRANSFER, true);
        
        $result = curl_exec($channel);
        $debug  = curl_errno($channel);
        
        $data = [
            'phone'       => $this->phone,
            'date'        => date('Y-m-d H:i:s'),
            'result'      => json_decode($result),
            'debug_error' => json_decode($debug),
        ];
        
        curl_close($channel);
        
        // Если ошибка в КТ, шлём письмо
        if ($data['result']->errorCode || $data['debug_error']) {
            
            $emails = [
                'r@sinergium.ru',
                'si@sinergium.ru',
            ];
            
            $emails_config = config('common.email_responsible');
            
            if ( ! empty($emails_config) && is_array($emails_config)) {
                foreach ($emails_config as $item) {
                    $emails[] = $item;
                }
            }
            
            send_mail($emails, 'Ошибка создания заявки CallTouch', '<pre>' . print_r($data, true) . '</pre>');
        }
        
        logger('calltouch', $data);
        
        return $result;
    }
}