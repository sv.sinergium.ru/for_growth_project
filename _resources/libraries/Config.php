<?php

/**
 * Class Config
 */
class Config
{
    /**
     * Инстанс
     *
     * @var Config|null
     */
    private static ?Config $instance = null;
    
    /**
     * Набор настроек
     *
     * @var array
     */
    private array $config;
    
    /**
     * Config constructor.
     */
    protected function __construct()
    {
        $this->loadConfigs();
    }
    
    /**
     * Загрузка конфигов
     */
    private function loadConfigs()
    {
        $configs = scandir(CONFIG_DIR);
        
        foreach ($configs as $config) {
            if ( ! is_dir(CONFIG_DIR . $config) && (strpos($config, '.php') !== false)) {
                $name                = rtrim($config, '.php');
                $this->config[$name] = include(CONFIG_DIR . $config);
            }
        }
    }
    
    /**
     * Получить инстанс конфига
     *
     * @return mixed|static
     */
    private static function getInstance()
    {
        return self::$instance ? self::$instance : self::$instance = new static();
    }
    
    /**
     * Получить конфиг
     *
     * @param string $name Ключ конфига
     *
     * @return mixed|null
     */
    public static function get(string $name)
    {
        $instance = static::getInstance();
        
        return get_array_value($instance->config, $name);
    }
    
    /**
     * Получить весь конфиг
     *
     * @return array
     */
    public static function all()
    {
        $instance = static::getInstance();
        
        return $instance->config;
    }
    
    protected function __clone()
    {
    }
    
    /**
     * @throws \Exception
     */
    public function __wakeup()
    {
        throw new \Exception('Cannot unserialize a singleton');
    }
}