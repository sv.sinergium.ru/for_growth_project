<?php

use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;

/**
 * Class Mail
 */
class Mail
{
    /**
     * @var array|string|null
     */
    private $email;
    
    /**
     * @var string|null
     */
    private ?string $subject;
    
    /**
     * @var string|null
     */
    private ?string $message;
    
    /**
     * @var string|null
     */
    private ?string $error = null;
    
    /**
     * Mail constructor.
     *
     * @param string|array $email
     * @param string $subject
     * @param string $message
     */
    public function __construct($email, string $subject, string $message)
    {
        $this->email   = $email;
        $this->subject = $subject;
        $this->message = $message;
    }
    
    public function send()
    {
        $mail = new PHPMailer(true);
        
        try {
            $mail->SMTPDebug = SMTP::DEBUG_OFF;
            $mail->isSMTP();
            $mail->SMTPAuth = true;
            $mail->CharSet  = PHPMailer::CHARSET_UTF8;
            
            if (is_dev()) {
                $mail->Host       = 'smtp.mailtrap.io';
                $mail->Port       = 2525;
                $mail->Username   = '1b92b82c76c907';
                $mail->Password   = '7b9f958b9b95cd';
                $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
            } else {
                $mail->Host       = 'smtp.yandex.ru';
                $mail->Port       = 465;
                $mail->SMTPAuth   = true;
                $mail->Username   = config('common.mail.smtp.name') ?? '';
                $mail->Password   = config('common.mail.smtp.pass') ?? '';
                $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
            }
            
            $mail->setFrom(config('common.mail.smtp.name'), config('common.mail.from_name') ?? 'Лендинг');
            
            if (is_array($this->email)) {
                foreach ($this->email as $email) {
                    $mail->addAddress($email);
                }
            } else {
                $mail->addAddress($this->email);
            }
            
            $mail->isHTML(true);
            $mail->Subject = $this->subject;
            $mail->Body    = $this->message;
            
            $mail->send();
            
        } catch (Exception $e) {
            $this->error = "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
        }
        
        return $this;
    }
    
    /**
     * Вернуть ошибку
     *
     * @return string|null
     */
    public function getError()
    {
        return trim($this->error);
    }
    
    /**
     * Есть ошибка?
     *
     * @return bool
     */
    public function hasError()
    {
        return $this->error && trim($this->error);
    }
}