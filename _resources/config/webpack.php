<?php

return [
    'build_path' => 'build',
    'https'      => env('WEBPACK_HTTPS'),
    'domain'     => env('WEBPACK_DOMAIN'),
    'port'       => env('WEBPACK_PORT'),
];