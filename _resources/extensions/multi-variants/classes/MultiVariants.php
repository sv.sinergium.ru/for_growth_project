<?php

/**
 * Class MultiVariants
 */
class MultiVariants
{
    private static array $instances = [];
    
    /**
     * Набор вариантов текста
     *
     * @var array
     */
    private array $variants = [];
    
    protected function __construct()
    {
    }
    
    protected function __clone()
    {
    }
    
    /**
     * Сохранить вариант
     *
     * @param string $scope Скоуп
     * @param mixed $variant Вариант
     */
    public static function updateVariantsSeed(string $scope, $variant)
    {
        static::getInstance()->variants[$scope] = $variant;
    }
    
    /**
     * Получить вариант
     *
     * @param string $scope Скоуп
     *
     * @return mixed
     */
    public static function getVariant(string $scope)
    {
        return get_array_value(static::getInstance()->variants, "$scope");
    }
    
    /**
     * Получить зерно кэширования
     *
     * @return string
     */
    public static function getVariantsSeed(): string
    {
        return sha1(json_encode(static::getInstance()->variants));
    }
    
    /**
     * @throws Exception
     */
    public function __wakeup()
    {
        throw new \Exception("Cannot unserialize a singleton.");
    }
    
    /**
     * @return MultiVariants
     */
    public static function getInstance(): MultiVariants
    {
        $instance = static::class;
        
        if ( ! isset(self::$instances[$instance])) {
            self::$instances[$instance] = new static();
        }
        
        return self::$instances[$instance];
    }
}