<?php

if ( ! function_exists('get_variant')) {
    
    /**
     * Получить вариант
     *
     * @param string $scope Скоуп
     *
     * @return string
     */
    function get_variant(string $scope): string
    {
        if ($variant = MultiVariants::getVariant($scope)) {
            return $variant;
        }
        
        $variant = null;
        
        foreach (config('multi-variants.' . $scope) as $key => $value) {
            if ($key === 'default') {
                continue;
            }
            
            if (stripos(get_array_value($_GET, 'utm_campaign'), $key) !== false) {
                $variant = $value;
            }
        }
        
        if ( ! $variant) {
            $variant = config('multi-variants.' . $scope . '.default');
        }
        
        MultiVariants::updateVariantsSeed($scope, $variant);
        
        return $variant;
    }
}

if ( ! function_exists('get_cache_salt')) {
    
    /**
     * Получить соль кэша
     *
     * @return string
     */
    function get_cache_salt(): string
    {
        foreach (config('multi-variants') as $scope => $variants) {
            $variants_array[] = get_variant($scope);
        }
        
        return MultiVariants::getVariantsSeed();
    }
}