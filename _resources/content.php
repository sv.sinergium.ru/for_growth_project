<?php

if ( ! function_exists('phone')) {
    
    /**
     * Нормализация телефона для ссылок типа tel:
     *
     * @param string $string
     *
     * @return null|string|string[]
     */
    function phone(string $string)
    {
        return preg_replace('~[^0-9\+]~', '', $string);
    }
}

if ( ! function_exists('get_array_value')) {
    
    /**
     * Получаем значение массива по ключу через точку (.)
     *
     * @param array  $array   Массив
     * @param string $value   Значение
     * @param null   $default Дефолтное значение
     *
     * @return array|mixed|string|null
     */
    function get_array_value(array $array, string $value, $default = null)
    {
        $parts  = explode('.', $value);
        $exists = true;
        
        foreach ($parts as $part) {
            
            if (isset($array[$part]) && ((!is_array($array[$part]) && trim($array[$part])) || is_array($array[$part]))) {
                $array = &$array[$part];
            } else {
                $exists = false;
                break;
            }
        }
        
        return $exists ? $array : $default;
    }
}

if ( ! function_exists('config')) {
    
    /**
     * Получить конфиг
     *
     * @param string $name Ключ через точку
     *
     * @return mixed
     */
    function config($name = '')
    {
        return Config::get($name);
    }
}

if ( ! function_exists("new_year")) {
    
    /**
     * Проверяет новогодний период 2 недели до и после
     *
     * @param
     *
     * @return true|false
     *
     */
    function new_year()
    {
        $date  = getdate(time());
        $month = $date["mon"];
        $day   = $date["mday"];
        
        return ($month === 12 && $day >= 15) || ($month === 1 && $day < 10);
    }
}