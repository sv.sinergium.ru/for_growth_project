<?php

if ( ! function_exists('get_extensions')) {
    
    /**
     * Получить список расширений
     *
     * @return array
     */
    function get_extensions(): array
    {
        $extensions = [];
        
        if (is_dir(EXTENSIONS_DIR)) {
            foreach (array_diff(scandir(EXTENSIONS_DIR), ['..', '.']) as $extension) {
                $extensions[] = EXTENSIONS_DIR . $extension;
            }
        }
        
        return $extensions;
    }
}

if ( ! function_exists('get_page_cache_key')) {
    
    /**
     * Получить кэш для страницы
     *
     * @param string $name Имя файла страницы
     * @param array $additional Доп соль
     *
     * @return string
     */
    function get_page_cache_key(string $name, array $additional = []): string
    {
        // Указываем дефолтную соль
        $salt   = array_merge($additional, config('cache.salt'));
        $salt[] = $name;
        
        foreach (get_extensions() as $extension) {
            $files = array_diff(scandir($extension), ['..', '.']);
            if (in_array('cache.php', $files)) {
                $salt[] = include_once $extension . '/cache.php';
            }
        }
        
        return sha1(json_encode($salt));
    }
}

if ( ! function_exists('spaceless')) {
    
    /**
     * Вырезаем все пробелы
     *
     * @param string $string
     *
     * @return string|string[]|null
     */
    function spaceless(string $string)
    {
        $expelled_tags = implode('|', [
            'textarea',
            'script',
            'pre',
            'style',
        ]);
        
        // Все пробелы/переносы конвертируются в одинарный
        $regexp = '~(?>[^\S]\s*|\s{2,})(?=[^<]*+(?:<(?!/?(?:' . $expelled_tags . ')\b)[^<]*+)*+(?:<(?>' . $expelled_tags . ')\b|\z))~Six';
        $result = preg_replace($regexp, ' ', $string);
        
        return ($result !== null) ? $result : $string;
    }
}

if ( ! function_exists('page')) {
    
    /**
     * Инклюд страницы
     *
     * @param string $name
     * @param array $variables
     */
    function page($name = '404', $variables = [])
    {
        $cache_name = get_page_cache_key($name, $variables);
        
        if (cache_exists($cache_name)) {
            $output = cache($cache_name);
        } else {
            $output    = null;
            $file_path = ROOT_DIR . '/_resources/pages/' . trim($name) . '.php';
            
            if (file_exists($file_path)) {
                extract($variables);
                ob_start();
                
                include $file_path;
                
                $output = ob_get_clean();
                $output = cache($cache_name, spaceless($output));
            }
        }
        
        echo $output;
    }
}

if ( ! function_exists('dd')) {
    
    /**
     * Dump data
     *
     * @param      $data
     * @param bool $die
     */
    function dd($data, $die = true)
    {
        echo '<pre style="background-color: #fffd29;">';
        
        if (is_bool($data)) {
            var_dump($data);
        } else {
            print_r($data);
        }
        
        echo '</pre>';
        
        if ($die) {
            die();
        }
    }
}

if ( ! function_exists('starts_with')) {
    
    /**
     * Determine if a given string starts with a given substring.
     *
     * @param string $haystack
     * @param string|array $needles
     *
     * @return bool
     */
    function starts_with(string $haystack, $needles)
    {
        foreach ((array) $needles as $needle) {
            if ($needle !== '' && substr($haystack, 0, strlen($needle)) === (string) $needle) {
                return true;
            }
        }
        
        return false;
    }
}

if ( ! function_exists('cache_exists')) {
    
    /**
     * Есть ли кэш?
     *
     * @param string $key Ключ Кэша
     * @param int $ttl TTL
     *
     * @return bool
     */
    function cache_exists(string $key, $ttl = 1440)
    {
        if (CACHE_DISABLED || is_dev()) {
            return false;
        }
        
        $file_path = ROOT_DIR . '/cache/' . trim($key);
        
        if (file_exists($file_path)) {
            $last_modified = @filemtime($file_path);
            
            if ($last_modified && (time() > ($last_modified + (60 * $ttl)))) {
                return false;
            }
        } else {
            return false;
        }
        
        return true;
    }
}

if ( ! function_exists('cache')) {
    
    /**
     * Кэширование
     *
     * @param string $key Ключ кэша
     * @param mixed $data Данные
     * @param int $ttl TTL
     *
     * @return false|string|null
     */
    function cache(string $key, $data = null, $ttl = 1440)
    {
        $file_path = ROOT_DIR . '/cache/' . trim($key);
        
        if (CACHE_DISABLED || is_dev()) {
            @unlink($file_path);
            
            return $data;
        }
        
        if (cache_exists($key, $ttl)) {
            return file_get_contents($file_path);
        } else {
            file_put_contents($file_path, $data, LOCK_EX);
        }
        
        return $data;
    }
}

if ( ! function_exists('img')) {
    
    /**
     * Картинка
     *
     * @param string $name Имя файла
     * @param string|null $module Имя модуля, если есть
     *
     * @return string
     */
    function img(string $name, $module = null)
    {
        $images_path = $module ? MODULES_DIR . $module . '/images/' : ROOT_DIR . '/_resources/assets/images/';
        
        $file_path = $images_path . $name;
        
        if (file_exists($file_path)) {
            $hash              = sha1_file($file_path);
            $ext               = '.' . pathinfo($file_path)['extension'];
            $build_images_path = ROOT_DIR . '/public/build/ii/';
            
            (is_dir($build_images_path)) or @mkdir($build_images_path, 0775);
            
            @copy($file_path, "{$build_images_path}{$hash}{$ext}");
            
            return "/build/ii/{$hash}{$ext}";
        }
        
        return '/images/image-not-found.svg';
    }
}

if ( ! function_exists('json')) {
    
    /**
     * Возврат JSON
     *
     * @param array $data Данные
     */
    function json(array $data)
    {
        header('Content-Type: application/json');
        echo json_encode($data);
    }
}

if ( ! function_exists('phone')) {
    
    /**
     * Нормализация телефона для ссылок типа tel:
     *
     * @param string $string
     *
     * @return null|string|string[]
     */
    function phone(string $string)
    {
        return preg_replace('~[^0-9\+]~', '', $string);
    }
}

if ( ! function_exists('mb_ucfirst')) {
    
    /**
     * Мультибайтовая ucfirst
     *
     * @param string $string
     * @param string $encoding
     * @param bool $lower_str_end
     *
     * @return string
     */
    function mb_ucfirst(string $string, $encoding = 'UTF-8', $lower_str_end = false)
    {
        $first_letter = mb_strtoupper(mb_substr($string, 0, 1, $encoding), $encoding);
        
        if ($lower_str_end) {
            $string_end = mb_strtolower(mb_substr($string, 1, mb_strlen($string, $encoding), $encoding), $encoding);
        } else {
            $string_end = mb_substr($string, 1, mb_strlen($string, $encoding), $encoding);
        }
        
        $string = $first_letter . $string_end;
        
        return $string;
    }
}

if ( ! function_exists('price')) {
    
    /**
     * Форматирование цены
     *
     * @param int $price
     * @param string $separator
     *
     * @return mixed
     */
    function price(int $price, $separator = ' ')
    {
        return number_format($price, 0, ',', $separator);
    }
}

if ( ! function_exists('place_hold_it')) {
    
    /**
     * Плейсхолдер изображения
     *
     * @param int $width
     * @param int $height
     * @param string $hex
     *
     * @return string
     */
    function place_hold_it(int $width, int $height, $hex = 'ffffff')
    {
        return "//placehold.it/{$width}x{$height}/{$hex}";
    }
}

if ( ! function_exists('plural')) {
    
    /**
     * Склонение
     *
     * @param int $n Число
     * @param array $forms Формы
     *
     * @return mixed
     */
    function plural(int $n, array $forms = [])
    {
        $n = abs(intval($n));
        
        return $n % 10 == 1 && $n % 100 != 11 ? $forms[0] : ($n % 10 >= 2 && $n % 10 <= 4 && ($n % 100 < 10 || $n % 100 >= 20) ? $forms[1] : $forms[2]);
    }
}

if ( ! function_exists('get_experience')) {
    
    /**
     * @param string|int $start_year
     * @param bool $with_postfix
     * @param false $plus
     *
     * @return string
     */
    function get_experience($start_year, $with_postfix = true, $plus = false)
    {
        $today = new DateTime();
        $start = new DateTime("{$start_year}-01-01");
        $diff  = date_diff($today, $start);
        $diff  = $diff->format('%y');
        
        if ($plus) {
            $diff .= '+';
        }
        
        if ($with_postfix) {
            $diff .= '&nbsp;' . plural($diff, ['год', 'года', 'лет']);
        }
        
        return $diff;
    }
}

if ( ! function_exists('offer_end_time')) {
    
    /**
     * Получить срок окончания акции
     *
     * @return false|string
     */
    function offer_end_time()
    {
        $date        = date('d.m.Y');
        $day_of_week = date('w', strtotime($date));
        
        $str_time = ($day_of_week > 0 && $day_of_week < 5) ? 'next monday' : 'next friday';
        
        $date = date('d.m.Y', strtotime($str_time));
        $time = rus_date($date);
        
        return "Акция до {$time}";
    }
}

if ( ! function_exists('rus_date')) {
    
    /**
     * Перевод даты
     *
     * @param string $date Дата
     * @param boolean $with_year Добавить год в конце?
     *
     * @return string
     */
    function rus_date(string $date, $with_year = false)
    {
        $date = explode('.', $date);
        
        switch ($date[1]) {
            case 1:
                $m = 'января';
                break;
            case 2:
                $m = 'февраля';
                break;
            case 3:
                $m = 'марта';
                break;
            case 4:
                $m = 'апреля';
                break;
            case 5:
                $m = 'мая';
                break;
            case 6:
                $m = 'июня';
                break;
            case 7:
                $m = 'июля';
                break;
            case 8:
                $m = 'августа';
                break;
            case 9:
                $m = 'сентября';
                break;
            case 10:
                $m = 'октября';
                break;
            case 11:
                $m = 'ноября';
                break;
            case 12:
                $m = 'декабря';
                break;
        }
        
        if ($with_year) {
            $result = (int) $date[0] . '&nbsp;' . $m . '&nbsp;' . (int) $date[2] . '&nbsp;г.';
        } else {
            $result = (int) $date[0] . '&nbsp;' . $m;
        }
        
        return $result;
    }
}

if ( ! function_exists('tpl')) {
    
    /**
     * Подключение файла с передачей переменных
     *
     * @param string $__file_path Путь до шаблона
     * @param array $__variables Переменные
     *
     * @return false|string|null
     */
    function tpl(string $__file_path, $__variables = [])
    {
        $__output = null;
        
        if (file_exists($__file_path)) {
            extract($__variables);
            ob_start();
            include $__file_path;
            $__output = ob_get_clean();
        }
        
        return $__output;
    }
}

if ( ! function_exists('tpl_module')) {
    
    /**
     * Подключение файла модуля
     *
     * @param string $module Папка модуля
     * @param array $variables Массив переменных
     *
     * @return false|string|null
     */
    function tpl_module(string $module, $variables = [])
    {
        $file = isset($variables['file']) ? $variables['file'] . '.php' : 'tpl.php';
        
        return tpl(MODULES_DIR . $module . '/' . $file, $variables);
    }
}

if ( ! function_exists('page_vars')) {
    
    /**
     * Мердж переменных для страницы
     *
     * @param $instance
     * @param $variables
     *
     * @return array
     */
    function page_vars($instance, $variables)
    {
        return array_merge($instance->getVariables(), $variables);
    }
}

if ( ! function_exists("call_touch")) {
    
    /**
     * Получение объекта заявки в КТ
     *
     * @param string $phone Номер телефона
     * @param string|null $session_id ID сессии КТ (window.call_value)
     *
     * @return CallTouch
     */
    function call_touch(string $phone, $session_id = null)
    {
        return new CallTouch($phone, $session_id);
    }
}

if ( ! function_exists('re_captcha_check')) {
    
    /**
     * Проверка ReCaptcha
     *
     * @param string $url URL для проверки
     *
     * @return bool|string
     */
    function re_captcha_check(string $url)
    {
        $ch = curl_init();
        
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        
        $data = curl_exec($ch);
        curl_close($ch);
        
        return $data;
    }
}

if ( ! function_exists('webpack')) {
    
    /**
     * Webpack
     *
     * @param string $extension
     * @param string $bundle
     *
     * @return mixed|string
     *
     * @throws Exception
     */
    function webpack(string $extension, $bundle = 'app')
    {
        static $manifest = null;
        
        if (is_null($manifest)) {
            
            $manifest_directory = ROOT_DIR . '/public/build';
            
            if ( ! $manifest) {
                if ( ! file_exists($manifest_path = ($manifest_directory . '/manifest.json'))) {
                    throw new Exception('The manifest.json does not exist.');
                }
                
                $manifest = @json_decode(file_get_contents($manifest_path), true);
            }
        }
        
        $bundle_file = null;
        
        if (isset($manifest["{$bundle}.{$extension}"])) {
            $bundle_file = $manifest["{$bundle}.{$extension}"];
        }
        
        if ($bundle_file) {
            
            if (is_dev() && ! file_exists(ROOT_DIR . '/public' . $bundle_file)) {
                
                $url = '';
                
                if ( ! preg_match('~^(http\:\/\/|https\:\/\/)~i', $bundle_file)) {
                    $secure = config('webpack.https');
                    $server = config('webpack.domain');
                    $port   = config('webpack.port');
                    
                    $url = 'http' . ($secure ? 's://' : '://') . $server . ':' . $port;
                }
                
                return $url . $bundle_file;
            }
            
            return $bundle_file;
        }
        
        throw new InvalidArgumentException("File {$bundle}.{$extension} not defined in asset manifest.");
    }
}

if ( ! function_exists('env')) {
    
    /**
     * Env
     *
     * @param string $name Имя переменной
     *
     * @return mixed|string|null
     */
    function env(string $name)
    {
        $handle = fopen(ROOT_DIR . '/.env', 'r');
        
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                $explode = explode('=', $line);
                if (trim($explode[0]) === $name) {
                    return trim($explode[1]);
                }
            }
            
            fclose($handle);
        }
        
        return null;
    }
}

if ( ! function_exists('is_dev')) {
    
    /**
     * Это DEV сервер?
     *
     * @return bool
     */
    function is_dev()
    {
        return env('APP_ENV') === 'local';
    }
}

if ( ! function_exists('module_view')) {
    
    /**
     * @param string $module Имя модуля без префикса
     * @param string $view Имя вьюхи
     * @param array $variables Переменные
     *
     * @return false|string|null
     */
    function module_view(string $module, string $view, array $variables)
    {
        if ((stripos($module, '/') !== false) || stripos($view, '/') !== false) {
            return null;
        }
        
        $view = MODULES_DIR . 'm-' . $module . '/views/' . $view . '.php';
        
        return file_exists($view) ? tpl($view, $variables) : null;
    }
}

if ( ! function_exists('logger')) {
    
    /**
     * Запись данных в лог
     *
     * @param string $name Имя файла лога
     * @param array $data Данные
     *
     * @return bool
     */
    function logger(string $name, array $data)
    {
        $logger = new Logger($name, $data);
        
        return $logger->save();
    }
}

if ( ! function_exists('send_mail')) {
    
    /**
     * Отправка письма
     *
     * @param string|array $email Email'ы для отправки
     * @param string $subject Тема письма
     * @param string $message Сообщение
     *
     * @return Mail
     */
    function send_mail($email, string $subject, string $message)
    {
        $mail = new Mail($email, $subject, $message);
        
        return $mail->send();
    }
}

if ( ! function_exists("affiliates_count")) {
    
    /**
     * Количество клиник
     *
     * @return string
     */
    function affiliates_count()
    {
        return MODERATE
            ? count(config("affiliates.moderate")) . " " . plural(count(config("affiliates.moderate")), ["клиника", "клиники", "клиник"])
            : count(config("affiliates.regular")) . " " . plural(count(config("affiliates.regular")), ["клиника", "клиники", "клиник"]);
    }
}

if ( ! function_exists("is_good_ua")) {
    
    /**
     * Это хороший User Agent?
     *
     * @return bool
     */
    function is_good_ua()
    {
        return ! isset($_SERVER["HTTP_USER_AGENT"]) || (
                (stripos($_SERVER["HTTP_USER_AGENT"], "Chrome-Lighthouse") === false)
                && (stripos($_SERVER["HTTP_USER_AGENT"], "Googlebot") === false)
                && (stripos($_SERVER["HTTP_USER_AGENT"], "Yandexbot") === false)
            );
    }
}

if ( ! function_exists("count_files")) {
    
    /**
     * Считает файлы в папке
     *
     * @param $path
     * @param $type .jpg .png
     *
     * @return false|int
     */
    function count_files($path, $type)
    {
        $dir = opendir($path);
        
        if (empty($dir)) {
            return false;
        }
        
        $i = 0;
        while (false !== ($file = readdir($dir))) {
            
            if (strpos($file, $type, 1)) {
                $i++;
            }
        }
        
        return $i;
    }
}

if ( ! function_exists('log_spam')) {
    
    /**
     * Отправляет спамное письмо на почту
     *
     * @param array  $post
     * @param array  $mail
     * @param string $score
     *
     */
    function log_spam(array $post, array $mail, string $score = 'undefined' )
    {
        
        $subject      = get_array_value($post, 'subject', config('common.mail.subject'));
        $phone        = get_array_value($post, 'id');
        $phone_hidden = get_array_value($post, 'phone');
        
        $message = "<b>Тема:</b> {$subject} <br />";
        $message .= "<b>Телефон:</b> {$phone} <br />";
        
        if (isset($post['name']) && trim($post['name'])) {
            $message .= '<b>Имя:</b> ' . trim($post['name']) . '<br />';
        }
        if (isset($post['comment']) && trim($post['comment'])) {
            $message .= '<b>Комментарий:</b> ' . trim($post['comment']) . '<br />';
        }
        
        $message .= '<br />*************<br />Заявка направлена в спам <br />';
        $message .= 'score: ' . $score . '<br />';
        $message .= "Телефон: {$phone_hidden} <br />";
        
        send_mail($mail, $subject, $message);
    }
}