<?php
/**
 * Страница 404
 */ ?>

<?= tpl(TPL_DIR . 'header.php', get_defined_vars()); ?>

    <?= tpl_module('m-header', [
        'not_menu'    => true,
        'active_logo' => true,
    ]); ?>

    <?= tpl_module('m-not-found'); ?>

