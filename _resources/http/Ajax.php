<?php

namespace App;

/**
 * Class Ajax
 *
 * @package App
 */
class Ajax
{
    /**
     * Получить модалку из модуля
     */
    public function modal()
    {
        $module = isset($_GET['module']) && trim($_GET['module']) ? $_GET['module'] : null;
        $view   = isset($_GET['view']) && trim($_GET['view']) ? $_GET['view'] : 'modal';
        $key    = isset($_GET['key']) && trim($_GET['key']) ? $_GET['key'] : 'key';
        
        $data = [];
        
        // @TODO check module and view
        
        if (preg_match('~^[a-z0-9_:]+$~isu', $key, $match)) {
            $data = ['key' => $key];
        }
        
        $result = module_view($module, $view, $data);
        
        return json([
            'success' => ! ! $result,
            'view'    => $result,
        ]);
    }
}