<?php

namespace App;

/**
 * Class Example
 *
 * @package App
 */
class Basic
{
    /**
     * Переменные
     *
     * @var array
     */
    private array $variables = [];
    
    /**
     * Basic constructor.
     */
    public function __construct()
    {
        // Стили
        try {
            $this->variables['css'] = webpack('css', 'app_css');
            $this->variables['js']  = webpack('js', 'app_js');
        } catch (\Exception $exception) {
            echo $exception->getMessage();
            die();
        }
    }
    
    /**
     * Получить переменные
     *
     * @return mixed
     */
    public function getVariables()
    {
        return $this->variables;
    }
    
    /**
     * Главная страница
     */
    public function index()
    {
        page('index', page_vars($this, [
            'title'       => 'Новая орбита',
            'description' => 'Центр имплантации и стоматологии',
        ]));
    }
    
    /**
     * Страница спасибо
     */
//    public function success()
//    {
//        page('success', page_vars($this, [
//            'title'       => 'Спасибо за заявку и доверие!',
//            'description' => 'Спасибо за заявку и доверие!',
//        ]));
//    }
    
    /**
     * 404
     */
    public function notFound()
    {
        header('HTTP/1.0 404 Not Found', true, 404);
        
        page('404', page_vars($this, [
            'title'       => 'Ничего не найдено',
            'description' => 'Ничего не найдено',
        ]));
    }
    
    /**
     * Политика конфиденциальности
     */
//    public function conditions()
//    {
//        header('Content-type: application/pdf');
//        header('Content-Disposition: inline; filename=Политика Конфиденциальности.pdf');
//
//        @readfile(ROOT_DIR . '/_resources/pdf/politika-konfidentsialnosti.pdf');
//    }
}
