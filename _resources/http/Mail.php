<?php

namespace App;

/**
 * Class Mail
 *
 * @package App
 */
class Mail
{
    /**
     * Отправка письма
     */
    public function send()
    {
        // Проверка ReCaptcha
        if ( ! $this->reCaptchaCheck($_POST)) {
            
            return json([
                'result'  => false,
                'message' => 'Кажется, что вы робот',
            ]);
        }
        
        
        
        if ( ! is_dev()) {
            // Отправка заявки в КТ
            $this->sendToCallTouch($_POST);
        }
        
        $redirect = 'success';
        $subject  = get_array_value($_POST, 'subject', config('common.mail.subject'));
        $phone    = get_array_value($_POST, 'id');
        $type     = get_array_value($_POST, 'form_type');
        
        
        if ( ! $phone) {
            return json([
                'result'  => false,
                'message' => 'Телефон обязателен для заполнения',
            ]);
        }
        
        $message = "<b>Тема:</b> {$subject} <br />";
        $message .= "<b>Телефон:</b> {$phone} <br />";
        
        if (isset($_POST['name']) && trim($_POST['name'])) {
            $message .= '<b>Имя:</b> ' . trim($_POST['name']) . '<br />';
        }
        
        if (isset($_POST['comment']) && trim($_POST['comment'])) {
            $message .= '<b>Комментарий:</b> ' . trim($_POST['comment']) . '<br />';
        }
        
        
        if ($type && $type === "quiz") {
            $question_1 = get_array_value($_POST, 'question_1');
            $question_2 = get_array_value($_POST, 'question_2');
            $question_3 = get_array_value($_POST, 'question_3');
            $question_4 = get_array_value($_POST, 'question_4');
            $question_5 = get_array_value($_POST, 'question_5');

            $message .= '<br /> <b>Ответы теста</b></b> <br>';
            $message .= '<b>Какая у вас ситуация: </b> ' . trim($question_1) . '<br />';
            $message .= '<b>Какие зубы требуется восстановить: </b> ' . trim($question_2) . '<br />';
            $message .= '<b>Как давно у вас эта проблема: </b> ' . trim($question_3) . '<br />';
            $message .= '<b>Есть ли у вас хронические заболевания: </b> ' . trim($question_4) . '<br />';
            $message .= '<b>Когда планируете начать лечение: </b> ' . trim($question_5) . '<br />';
            
            $is_modal = get_array_value($_POST, 'modal_quiz');

//            if ($is_modal) {
//                $redirect = false;
//            }
        }
        
        // Отправка письма
        $result = send_mail(config('common.email'), $subject, $message);
        
        if ($result->hasError()) {
            return json([
                'result'  => false,
                'message' => $result->getError(),
            ]);
        }
        
        return json([
            'result'   => true,
            'redirect' => $redirect,
        ]);
    }
    
    /**
     * Проверка ReCaptcha
     *
     * @param array $post $_POST
     *
     * @return bool
     */
    public function reCaptchaCheck(array $post)
    {
        $token = get_array_value($post, 'g-recaptcha-response');
        
        if ( ! $token) {
            return false;
        }
        
        $response = re_captcha_check('https://www.google.com/recaptcha/api/siteverify?secret=' . config('common.re_captcha.secret') . '&response=' . $token);
        
        if ( ! $response) {
            return false;
        }
        
        $data = json_decode($response);
        
        if ( ! $data->success || ($data->score < 0.5)) {
            
            // Если поле пустое скорее всего тупит рекапча
            if ( ! get_array_value($post, 'phone')) {
                return true;
            }
            
            // лог проверки на спам
            log_spam($post, ['si@sinergium.ru'], $data->score ?? false);
            
            return false;
        }
        
        return true;
    }
    
    /**
     * Отправка CallTouch заявки
     *
     * @param array $post $_POST
     */
    public function sendToCallTouch(array $post)
    {
        $tags       = isset($post['tags']) ? explode(',', $post['tags']) : '';
        $phone      = get_array_value($post, 'id');
        $name       = get_array_value($post, 'name');
        $comment    = get_array_value($post, 'comment');
        $page_url   = get_array_value($post, 'page_url');
        $session_id = get_array_value($post, 'session_id');
        $subject    = get_array_value($post, 'subject', config('common.mail.subject'));
        
        $call_touch = call_touch($phone, $session_id);
        
        if ($name) {
            $call_touch->setFio($name);
        }
        
        if ($comment) {
            $call_touch->setComment($comment);
        }
        
        if ($page_url) {
            $call_touch->setPage($page_url);
        }
        
        if ($tags) {
            $call_touch->setTags($tags);
        }
        
        $call_touch->setSubject($subject);
        $call_touch->createRequest();
    }
}
