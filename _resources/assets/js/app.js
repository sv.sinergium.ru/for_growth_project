window.jQuery = window.$ = require('jquery');

import Base from 'js/_base-controller';
//Плагины
import 'slick-carousel';
import 'magnific-popup';

//Основные

import ScrollTo        from 'js/modules/scroll-to';
import ScrollToTop     from 'js/modules/scroll-to-top';
import Ajax            from 'js/modules/ajax';
import Lazyload        from 'js/modules/lazyload';
import Tooltip         from 'js/modules/tooltip';

if (__DEV__) {
    require('css/app.scss');
}

class ContextApp extends Base {
    
    /**
     * @returns {boolean}
     * @private
     */
    _init() {
        
        let window    = require('window');
        window.jQuery = $;
        window.$      = $;
        
        /**  Основные  **/
        // Ajax
        new Ajax();
        // Lazy images
        new Lazyload();
        // Тултипы
        new Tooltip();
        
        
        // Прокрутка к элементам
        this.scrollTo = new ScrollTo({
            mobile_width: 1024,
            mobile_hh: 100,
            mobile_hh_small: 50,
            pc_hh: 150,
            pc_hh_small: 70,
        });
        
        // Прокрутка наверх
        new ScrollToTop({
            scroll_speed: 700,
            show_threshold: 2000,
        });
        
        return true;
    }
    
    _bind() {
        
        // Подключает к кнопкам коллтач.
        let callTouch;
        
        let waitUntilCt = () => {
            callTouch = this._nullSafe(() => window.Calltouch.Callback);
            
            if (callTouch) {
                clearInterval(ct_interval);
                
                let $buttons = $('[data-js-cbs]');
                $buttons.off('click');
                
                this._bindTo($buttons, 'click', (ev) => {
                    ev.preventDefault();
                    callTouch.onClickCallButton();
                });
                
            } else {
                console.info('Коллтач не найден');
            }
        };
        
        waitUntilCt();
        
        let ct_interval = setInterval(() => {
            waitUntilCt();
        }, 100);
        
        setTimeout(() => {
            clearInterval(ct_interval);
        }, 30000);
        
        return true;
    }
}

window.ContextApp = new ContextApp();
