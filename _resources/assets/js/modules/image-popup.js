import Base from 'js/_base-controller';

export default class ImagePopup extends Base {
    
    /**
     * Инициализация
     *
     * @returns {boolean}
     */
    _init() {
        
        this.selector = '[data-js="image-popup"]';
        
        this.close = 'Закрыть (Esc)';
        this.load  = 'Загрузка… (%curr% из %total%)';
        
        return true;
    }
    
    /**
     * Бинд событий
     *
     * @returns {boolean}
     */
    _bind() {
        
        this._liveBindTo(this.selector, 'click', (e) => {
            e.preventDefault();
            
            let $current = $(e.currentTarget),
                $root    = $current.closest('[data-js="image-popup-gallery"]');
            
            if ($root.length) {
                
                let $items = $root.find('[data-js="image-popup"]'),
                    items  = [];
                
                if ($root.data('image-gallery-initialized')) {
                    $items.magnificPopup('goTo', $current.index());
                    return;
                }
                
                $items.each((i, e) => {
                    let obj = {
                        src: $(e).attr('data-src'),
                    };
                    
                    items.push(obj);
                });
                
                $items.magnificPopup({
                    tClose: this.close,
                    tLoading: this.load,
                    type: 'image',
                    gallery: {
                        enabled: true,
                        navigateByImgClick: true,
                        tPrev: 'Назад (Стрелка влево)',
                        tNext: 'Вперёд (Стрелка вправо)',
                        tCounter: '<span class="mfp-counter">%curr% из %total%</span>',
                    },
                    items: items,
                    callbacks: {
                        beforeOpen: function() {
                            this.index = $current.index();
                        }
                    }
                }).magnificPopup('open');
                
                $root.data('image-gallery-initialized', true);
                
            } else {
                $.magnificPopup.open({
                    tClose: this.close,
                    tLoading: this.load,
                    mainClass: 'mfp-fade mfp_image-popup',
                    removalDelay: 160,
                    preloader: false,
                    fixedContentPos: false,
                    items: {
                        src: $current.attr('data-src'),
                        type: 'image',
                    },
                });
            }
            
            return false;
        });
        
        return true;
    }
}
