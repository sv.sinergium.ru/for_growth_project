import Base from 'js/_base-controller';

export default class Goals extends Base {
    
    /**
     * @returns {boolean}
     */
    _init() {
        
        this.goals = '[data-js-goal]';
        this.ym_id = $('[name="ym_id"]').attr('content');
        
        return true;
    }
    
    /**
     * @returns {boolean}
     */
    _bind() {
        
        this._bindTo($(window), 'goals:presented', (e, data) => {
            data.$goals.each((i, el) => {
                let $el   = $(el),
                    event = $el.data('js-goal'),
                    data  = $el.data('js-goal-data');
                
                this._bindTo($el, event, () => {
                    this.reachGoal($el, data);
                });
                
                $el.attr('data-golas-binded', true);
            });
        });
        
        this.waitGoals();
        
        return true;
    }
    
    /**
     * Ждём появления целей
     */
    waitGoals() {
        let $goals = $(`${this.goals}:not([data-golas-binded])`);
        
        if ($goals.length) {
            $(window).trigger('goals:presented', { $goals: $goals });
        }
        
        setTimeout(() => this.waitGoals(), 250);
    }
    
    /**
     * Цель Яндекс.Метрика
     *
     * @param $el
     * @param data
     */
    reachGoal($el, data) {
        
        if (data && $el.length && ! $el.data('js-goal-init')) {
            let ym_goal = this._nullSafe(() => data.ym),
                gl_goal = this._nullSafe(() => data.gl.action),
                gl_cat  = this._nullSafe(() => data.gl.category);
            
            if (ym_goal) {
                // Отправляем в Яндекс Метрику
                let ym = this._nullSafe(() => window[`yaCounter${this.ym_id}`]);
                
                if (ym) {
                    ym.reachGoal(ym_goal);
                }
            }
            
            if (gl_goal && gl_cat) {
                // Отправляем в Гугл Аналитику
                let ga = this._nullSafe(() => window.ga);
                
                if (ga) {
                    let tracker = ga.getAll()[0];
                    
                    if (tracker) {
                        tracker.send('event', gl_cat, gl_goal);
                    }
                }
            }
            
            $el.attr('data-js-goal-init', true);
        }
    }
}