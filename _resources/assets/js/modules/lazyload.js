import Base from 'js/_base-controller';

export default class Lazyload extends Base
{
    /**
     * Инициализация
     *
     * @returns {boolean}
     * @private
     */
    _init() {
        this.$images = $('[data-js="lazy"]');
        this.in_advance = 500;
        
        return true
    }
    
    /**
     * Бинд событий
     *
     * @returns {boolean}
     * @private
     */
    _bind() {
        
        this._bindTo($(window), 'load', () => {
            this.lazyImages();
            
            this._bindTo($(window), 'scroll', () => {
                this.lazyImages();
            });
        });
        
        
        
        return true;
    }
    
    lazyImages() {
        
        this.$images.each((i, el) => {
            let image = el;
            
            if ($(image).offset().top < window.innerHeight + window.pageYOffset + this.in_advance && !image.classList.contains('loaded')) {
                image.src = image.dataset.src;
                image.onload = () => image.classList.add('loaded');
                
                if($(image).closest('video').length > 0) {
                    let video = image.parentElement;
                    
                    if(!$(video).is('.loaded')) {
                        video.load();
                        video.play();
                    }
                    
                    $(video).addClass('loaded');
                }
            }
        });
    }
}
