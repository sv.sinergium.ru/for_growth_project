'use strict';

import Base from 'js/_base-controller';

export default class FormsCtSession extends Base {
    
    /**
     * Инициализация
     *
     * @returns {boolean}
     */
    _init() {
        
        this.$forms  = $('[data-js="ajax-form"]');
        
        return ! ! this.$forms.length;
    }
    
    /**
     * Бинд событий
     *
     * @returns {boolean}
     */
    _bind() {
        
        // Добавляем к формам session_id
        this._bindTo($(window), 'ct:loaded', () => {
            this.$forms.each((i, el) => {
                $(el).append('<input type="hidden" name="session_id" value="' + window.call_value + '">');
                $(el).append('<input type="hidden" name="page_url" value="' + window.location.href + '">');
            });
        });
    
        // Ждём пока session_id появится
        function waitUntil() {
            if ( ! window.call_value) {
                setTimeout(waitUntil, 100);
            } else {
                $(window).trigger('ct:loaded');
            }
        }
        
        waitUntil();
        
        return true;
    }
}
