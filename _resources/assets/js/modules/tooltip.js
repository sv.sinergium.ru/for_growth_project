import Base  from 'js/_base-controller';
import tippy from 'tippy.js';

export default class Tooltip extends Base {
    
    /**
     * Инициализация
     *
     * @returns {boolean}
     */
    _init() {
        
        this.selector = '[data-js="tooltip-button"]';
        
        return true;
    }
    
    /**
     * Бинд событий
     *
     * @returns {boolean}
     */
    _bind() {
        
        // По клику ищем все попперы и биндим
        this._liveBindTo(this.selector, 'click', (e) => {
            let $target = $(e.currentTarget);
            
            $target.each((i, el) => {
                let $html  = $(el).closest('[data-js="tooltip"]').find('[data-js="tooltip-html"]'),
                    $theme = $(el).closest('[data-js="tooltip"]').attr('data-js-theme');
                
                // Если уже инициализирован, то пропускаем
                if (el._tippy) {
                    return true;
                }
                
                // Формируем темы
                if ($theme) {
                    $theme = $theme + ' light rounded';
                } else {
                    $theme = 'light rounded';
                }
                
                const self = this;
                
                tippy(el, {
                    arrow: true,
                    arrowType: 'sharp',
                    hideOnClick: true,
                    html: $($html.html()).get(0),
                    theme: $theme,
                    trigger: 'click',
                    interactive: true,
                    size: 'large',
                    animation: 'shift-away',
                    distance: '20',
                    placement: 'bottom',
                    inertia: true,
                    onShow(instance) {
                        let $popper = $(instance.popper),
                            $arrow  = $popper.find('.tippy-arrow');
                        
                        // Если ещё не добавлии наши элементы управления, то добавляем
                        if ( ! $arrow.find('[data-js-tooltip="arrow"]').length) {
                            
                            // Уголок
                            $arrow.append(
                                $('<div/>', {'data-js-tooltip': 'arrow'}),
                            );
                            
                            // Кнопка закрытия
                            $popper.find('.tippy-content').append(
                                $('<div/>', {'data-js-tooltip': 'close'}),
                            );
                            
                            $popper.find('[data-js-tooltip="close"]').html(
                                '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 18" width="14" height="14"><path d="M11.6 9.3c-.2-.2-.2-.4 0-.6l6-6c.2-.2.2-.4.2-.6s-.1-.4-.2-.6L16.5.4c-.2-.2-.4-.2-.6-.2-.2 0-.4.1-.6.2l-6 6c-.2.2-.4.2-.6 0l-6-6C2.5.2 2.3.2 2.1.2c-.2 0-.4.1-.6.2L.4 1.5c-.2.2-.2.4-.2.6s.1.4.2.6l6 6c.2.2.2.4 0 .6l-6 6c-.2.2-.2.4-.2.6 0 .2.1.4.2.6l1.1 1.1c.2.2.4.2.6.2.2 0 .4-.1.6-.2l6-6c.2-.2.4-.2.6 0l6 6c.2.2.4.2.6.2.2 0 .4-.1.6-.2l1.1-1.1c.2-.2.2-.4.2-.6 0-.2-.1-.4-.2-.6l-6-6z" fill="#e0e3e4"/></svg>',
                            );
                            
                            self._bindTo($popper.find('[data-js-tooltip="close"]'), 'click', () => {
                                $popper.get(0)._tippy.hide();
                            });
                        }
                        
                        // При ресайзе прячем тултип
                        self._bindTo($(window), 'resize orientationchange tippy:close', () => {
                            $popper.get(0)._tippy.hide();
                        });
                    },
                });
                
                $html.remove();
            });
            
            $target.get(0)._tippy.show();
        });
        
        return true;
    }
}
