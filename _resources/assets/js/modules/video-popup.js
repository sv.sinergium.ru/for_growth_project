import Base from 'js/_base-controller';

export default class VideoPopup extends Base {
    
    /**
     * Инициализация
     *
     * @returns {boolean}
     */
    _init() {
        
        this.selector = '[data-js="video-popup"]';
        
        return true;
    }
    
    /**
     * Бинд событий
     *
     * @returns {boolean}
     */
    _bind() {
        
        this._liveBindTo(this.selector, 'click', (e) => {
            e.preventDefault();
            
            let $current = $(e.currentTarget),
                src      = $current.data('js-video-popup') ? $current.data('js-video-popup') : $current.attr('href');
            
            src = src.replace('&feature=youtu.be', '');
            
            $.magnificPopup.open({
                tClose: 'Закрыть (Esc)',
                tLoading: 'Загрузка… (%curr% из %total%)',
                type: 'iframe',
                mainClass: 'mfp-fade mfp_video-popup',
                removalDelay: 160,
                preloader: false,
                fixedContentPos: false,
                items: {
                    src: src,
                },
                iframe: {
                    markup: `<div class="mfp-iframe-scaler"><div class="mfp-close"></div><iframe class="mfp-iframe" frameborder="0" allow="autoplay"></iframe></div>`,
                    patterns: {
                        youtube: {
                            src: '//www.youtube.com/embed/%id%?rel=0&autoplay=1',
                        },
                    },
                },
            });
            
            return false;
        });
        
        return true;
    }
}
