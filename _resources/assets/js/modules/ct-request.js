import Base from 'js/_base-controller';

export default class CTRequest extends Base {
    
    /**
     * Инициализация
     *
     * @returns {boolean}
     */
    _init() {
        
        this.$form  = $('[data-js="ajax-form"]');
        this.widget = 'callback'; // Создаётся в ЛК КТ «Форма на сайте»
        
        return !! this.$form.length;
    }
    
    /**
     * Бинд событий
     *
     * @returns {boolean}
     */
    _bind() {
        
        this._bindTo($(window), 'ctw:loaded', () => {
            
            this.$form.each((i, e) => {
                let $form = $(e);
    
                this.initRequest($form);
            });
    
            this._bindTo($(window), 'modal:opened', (ev, data) => {
                let $form  = $(data).find('[data-js="ajax-form"]');
    
                this.initRequest($form);
            });
        });
        
        // Ждём пока появится Виджет КТ
        function waitUntil() {
            if ( ! window.ctw) {
                setTimeout(waitUntil, 100);
            } else {
                $(window).trigger('ctw:loaded');
            }
        }
        
        waitUntil();
        
        return true;
    }
    
    /**
     * Отправка ОЗ
     *
     * @param $form
     */
    request($form) {
        let $input = $form.find('input[name="id"]'),
            phone  = $input.val().replace(/[^0-9]/g, '');
        
        if (phone.length === 10) {
            phone = `7${phone}`;
        }
        
        if ((phone.length === 11) && /^8/.test(phone)) {
            phone = phone.replace(/^8/, '7');
        }
        
        window.ctw.createRequest(this.widget, phone, [], (success, data) => {
            if (success) {
                console.log('Создана заявка на коллбэк, идентификатор: ' + data.callbackRequestId);
                
                // Если ОК, то редиректим
                document.location.href = `${document.location.origin}/success`;
            } else {
                switch (data.type) {
                    case 'request_throttle_timeout':
                    case 'request_throttle_count':
                        console.error('Достигнут лимит создания заявок, попробуйте позже');
                        
                        break;
                    case 'request_phone_blacklisted':
                        console.error('Номер телефона находится в чёрном списке');
                        
                        break;
                    case 'validation_error':
                        console.error('Были переданы некорректные данные');
                        
                        break;
                    default:
                        console.error('Во время выполнения запроса произошла ошибка: ' + data.type);
                }
                
                // Если фейл шлём форму
                $form.trigger('submit');
            }
        });
    }
    
    /**
     * Инициализация формы КТ
     *
     * @param $form
     */
    initRequest($form){
    
        window.ctw.getRouteKeyData(this.widget, (success, data) => {
            if ( ! success) {
                console.error('Во время обработки произошла ошибка');
                return;
            }
        
            let working = this._nullSafe(() => data.widgetData.callCenterWorkingMode);
        
            if ( ! working) {
                console.error('Не найден включённый виджет по routeKey, либо услуга обратного звонка не активна');
                return;
            }
        
            if (working !== 'working_hours') {
                console.error('Колл-центр не работает, заявки в нерабочее время не собираем');
                return;
            }
        
            console.info('Колл-центр работает');
        
            let $submit = $form.find('[type="submit"]');
        
            // Пробуем отправить заявку на обратный звонок
            this._bindTo($submit, 'click', (ev) => {
                ev.preventDefault();
            
                $submit.prop('disabled', true);
            
                this.request($form);
            
                return false;
            });
        });
    }
}