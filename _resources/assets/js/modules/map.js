import Base from 'js/_base-controller';

export default class Map extends Base {
    
    /**
     * Инициализация
     *
     * @returns {boolean}
     */
    _init() {
        
        this.$map        = $('#map');
        this.$map_loader = $('[data-js="map_loader"]');
        
        this.points = this.$map.data('js-markers');
        
        return !! this.$map.length;
    }
    
    /**
     * Бинд событий
     *
     * @returns {boolean}
     */
    _bind() {
        
        this._bindTo($(window), 'scroll resize orientationchange', () => {
            if (this.mapInViewport()) {
                this.initMap();
            }
        });
        
        return true;
    }
    
    /**
     * Карта во вьюпорте?
     *
     * @returns {boolean|boolean}
     */
    mapInViewport() {
        let map   = this.$map.get(0),
            rect  = map.getBoundingClientRect(),
            delta = map.clientHeight + 1000;
        
        return (((rect.top + delta) >= 0) && ((rect.bottom - delta) <= (window.innerHeight || document.documentElement.clientHeight)));
    }
    
    /**
     * Инициализация карты
     */
    initMap() {
        
        if (this.$map.data('map-initiated')) {
            this._unbindFrom($(window), 'scroll resize orientationchange');
            return;
        }
        
        $('body').append(
            $('<script/>', {src: 'https://api-maps.yandex.ru/2.1/?apikey=ece8937c-d56d-48cc-a3a4-263407324171&lang=ru_RU'}),
        );
        
        let waitYMaps = setInterval(() => {
            if (window.ymaps && window.ymaps.Map) {
                clearInterval(waitYMaps);
                ymaps.ready(init.bind(this));
            }
        }, 100);
        
        function init() {
            this.$map_loader.remove();
            window.__map = new ymaps.Map('map', {
                center: [55.76413821890915, 37.61385844970701],
                zoom: 12,
                controls: [],
            });
            
            window.__map.controls.add('zoomControl', {
                position: {
                    left: 'auto',
                    right: 10,
                    top: 108,
                    bottom: 'auto',
                },
            });
            
            window.__map.behaviors.disable('scrollZoom');
            
            if (this._isTouchDevice()) {
                window.__map.behaviors.disable('drag');
            }
            
            let myCollection = new ymaps.GeoObjectCollection();
            
            $.each(this.points, (a, i) => {
                
                let pin_icon   = '/images/pin.svg',
                    pin_size   = [50, 50],
                    pin_offset = [-25, -50];
                
                let marker = new ymaps.Placemark([i.lat, i.lng], {
                    hintContent: i.hint_content,
                }, {
                    iconLayout: 'default#image',
                    iconImageHref: pin_icon,
                    iconImageSize: pin_size,
                    iconImageOffset: pin_offset,
                });
                
                
                myCollection.add(marker);
            });
            
            window.__map.geoObjects.add(myCollection);
            window.__map.setBounds(myCollection.getBounds(), {
                checkZoomRange: true,
                zoomMargin: [85, 35],
            });
        }
        
        this.$map.data('map-initiated', true);
    }
}
