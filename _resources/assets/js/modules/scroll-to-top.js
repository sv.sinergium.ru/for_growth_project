import Base from 'js/_base-controller';

export default class ScrollToTop extends Base {
    
    /**
     * Инициализация
     *
     * @returns {boolean}
     */
    _init() {
        
        this.$button = $('.scroll-to-top');
        
        if (! this.$button.length) {
            return false;
        }
        
        this.scroll_speed   = this.options.scroll_speed || 300;
        this.show_threshold = this.options.show_threshold || 480;
        
        return true;
    }
    
    /**
     * Бинд событий
     *
     * @returns {boolean}
     */
    _bind() {
        
        // Скролл страницы
        this._bindTo($(window), 'scroll', () => {
            let height = $(window).scrollTop();
            
            if (height > this.show_threshold) {
                this.$button.addClass('scroll-to-top_shown');
            } else {
                this.$button.removeClass('scroll-to-top_shown');
            }
        });
        
        // Скролл клик
        this._bindTo(this.$button, 'click', () => {
            
            $('body, html').stop().animate({
                scrollTop: 0,
            }, this.scroll_speed);
        });
        
        return true;
    }
}