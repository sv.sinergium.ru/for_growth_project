import Base from 'js/_base-controller';

export default class Ajax extends Base {
    
    _init() {
        
        this.selector = '[data-ajax]';
        
        return true;
    }
    
    _bind() {
        
        this._liveBindTo(this.selector, 'click', (ev) => {
            let $current = $(ev.currentTarget);
            
            $current.prop('disabled', true);
            
            $.ajax({
                url: $(ev.currentTarget).data('ajax'),
                success: (response, text_status, jqXHR) => {
                    let success = this._nullSafe(() => response.success);
                    
                    if (success) {
                        let view = this._nullSafe(() => response.view);
                        
                        $(window).trigger('view:fetched', $(view));
                    }
                    
                    $current.prop('disabled', false);
                },
                error: (jqXHR, text_status, error_thrown) => {
                    $current.prop('disabled', false);
                },
                complete: (jqXHR, text_status) => {
                    $current.prop('disabled', false);
                },
            });
        });
        
        return true;
    }
}
