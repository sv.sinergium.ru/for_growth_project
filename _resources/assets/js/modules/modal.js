import Base from 'js/_base-controller';

export default class Modal extends Base {
    
    _init() {
        
        return true;
    }
    
    _bind() {
        
        this._bindTo($(window), 'view:fetched', (ev, data) => {
            this.render(data);
        });
        
        return true;
    }
    
    /**
     * Рендер модалки
     *
     * @param $target
     */
    render($target) {
        
        $.magnificPopup.open({
            tClose: 'Закрыть (Esc)',
            tLoading: 'Загрузка… (%curr% из %total%)',
            mainClass: 'mfp-fade mfp_modal',
            removalDelay: 10,
            autoFocusLast: false,
            items: {
                src: $target,
                type: 'inline',
            },
            callbacks: {
                beforeOpen: function () {
                    $('html').css('overflow', 'hidden');
                },
                open: () => {
                    $(window).trigger('modal:opened', $target);
                },
                afterClose: () => {
                    $('html').removeAttr('style');
                }
            },
        });
        
        $('.services__modal-anchor').on('click', function () {
            $.magnificPopup.close();
        });
    }
    
    
}
