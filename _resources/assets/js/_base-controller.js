'use strict';

if (Function.prototype.name === undefined && Object.defineProperty !== undefined) {
    Object.defineProperty(Function.prototype, 'name', {
        get: function () {
            let funcNameRegex = /function\s([^(]{1,})\(/;
            let results       = (funcNameRegex).exec((this).toString());
            return (results && results.length > 1) ? results[1].trim() : '';
        },
        set: function (value) {
        },
    });
}

/**
 * Дефолтный контроллер
 */
export default class Base {
    
    /**
     * @param options
     */
    constructor(options = {}) {
        
        try {
            this._construct(options);
        } catch (error) {
            console.error(error);
        }
    }
    
    /**
     * Постройка класса
     *
     * @private
     */
    _construct(options = {}) {
        this.options = options;
        this.modules = {};
        
        this.initialized = this._init();
        
        if (this.initialized) {
            this.binded = this._bind();
        }
        
        return this;
    }
    
    /**
     * @returns {boolean}
     */
    _init() {
        return true;
    }
    
    /**
     * @returns {boolean}
     */
    _bind() {
        return true;
    }
    
    /**
     * @param $element
     * @param event_name
     * @param callback
     */
    _bindTo($element, event_name, callback) {
        $element.each((i, e) => {
            let $elem = $(e);
            
            if (! $elem.length || ! event_name) {
                return;
            }
            
            let event = this._parseEventName(event_name);
            
            $elem.off(event).on(event, (e, p) => {
                if ($.isFunction(callback)) {
                    callback(e, p);
                }
            });
        });
    }
    
    /**
     * По двупальцевому тапу
     *
     * @param $element
     * @param callback
     */
    _onTwoFingersTap($element, callback) {
        let event_name = 'touchstart';
        
        $element.each((i, e) => {
            let $elem = $(e);
            
            if (! $elem.length) {
                return;
            }
            
            $elem
                .off(`${event_name}.${this.constructor.name}`)
                .on(`${event_name}.${this.constructor.name}`, (e, p) => {
                    if ((e.touches.length > 1) && $.isFunction(callback)) {
                        callback(e, p);
                    }
                });
        });
    }
    
    /**
     * @param selector
     * @param event_name
     * @param callback
     */
    _liveBindTo(selector, event_name, callback) {
        if (! $.trim(selector) || ! event_name) {
            return;
        }
        
        let event = this._parseEventName(event_name);
        
        $(document, window).off(event, selector).on(event, selector, (e, p) => {
            if ($.isFunction(callback)) {
                callback(e, p);
            }
        });
    }
    
    /**
     * @param $element
     */
    _putCursorToEndOnFocus($element) {
        
        if (! $element.length) {
            return false;
        }
        
        this._bindTo($element, 'focus', (e) => {
            e.target.value = e.target.value;
        });
    }
    
    /**
     * @param $element
     * @param event_name
     */
    _unbindFrom($element, event_name) {
        
        $element.each((i, e) => {
            let $elem = $(e);
            
            if (! $elem.length || ! event_name) {
                return;
            }
            
            let event = this._parseEventName(event_name);
            
            $elem.off(event);
        });
    }
    
    /**
     * Скролл к элементу
     *
     * @param $element
     * @param header_height
     * @param callback
     * @param speed
     * @param easing
     */
    _scrollTo($element, header_height = 0, callback = null, speed = 300, easing = 'swing') {
        
        if (! $element.length) {
            return false;
        }
        
        if ($element.is('.scrolled')) {
            return false;
        }
        
        $element.addClass('scrolled');
        
        let scroll;
        
        scroll = parseInt($element.offset().top) - header_height;
        
        $('html, body').animate({
            scrollTop: scroll,
        }, speed, easing, () => {
        }).promise().then(() => {
            if ($.isFunction(callback)) {
                callback();
            }
            
            $element.removeClass('scrolled');
        });
    }
    
    /**
     * @param callback
     * @param wait
     * @param context
     *
     * @returns {function()}
     */
    _debounce(callback, wait = 150, context = this) {
        let timeout      = null,
            callbackArgs = null;
        
        const later = () => callback.apply(context, callbackArgs);
        
        return function () {
            callbackArgs = arguments;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
        }
    }
    
    /**
     * Null Safe
     *
     * @param chain_func
     * @returns {*}
     */
    _nullSafe(chain_func) {
        let result;
        
        try {
            result = chain_func();
        } catch (e) {
            
        }
        
        return result;
    }
    
    /**
     * Это объект?
     *
     * @param val mixed
     * @returns {boolean}
     */
    _isObject(val) {
        return val instanceof Object;
    }
    
    /**
     * Склонение существительных
     *
     * @param n string|int
     * @param forms array
     *
     * @returns string
     */
    _plural(n, forms) {
        return forms[n % 10 === 1 && n % 100 !== 11 ? 0 : n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20) ? 1 : 2];
    }
    
    /**
     * Форматирование цены
     *
     * @param int
     * @param s
     * @param n
     * @param x
     * @param c
     */
    _price(int, s = ' ', n = 0, x = 0, c = '.') {
        let re  = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
            num = int.toFixed(Math.max(0, ~ ~ n));
        
        return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
        
    }
    
    /**
     * Апдейт query параметра
     *
     * @param uri string
     * @param key string
     * @param value string
     *
     * @returns {string}
     */
    _updateQueryStringParameter(uri, key, value) {
        let re        = new RegExp('([?&])' + key + '=.*?(&|$)', 'i'),
            separator = uri.indexOf('?') !== - 1 ? '&' : '?';
        
        if (uri.match(re)) {
            return uri.replace(re, '$1' + key + '=' + value + '$2');
        } else {
            return uri + separator + key + '=' + value;
        }
    }
    
    /**
     * Получить хэш параметры
     *
     * @returns {{}}
     */
    _getHashParams() {
        let hash_params = {};
        
        let e,
            a = /\+/g,
            r = /([^&;=]+)=?([^&;]*)/g,
            q = window.location.hash.substring(1),
            d = function (s) {
                return decodeURIComponent(s.replace(a, ' '));
            };
        
        while (e = r.exec(q)) {
            hash_params[d(e[1])] = d(e[2]);
        }
        
        return hash_params;
    }
    
    /**
     * Удаление хэша из URL
     */
    removeHashParams() {
        history.pushState('', document.title, window.location.pathname + window.location.search);
    }
    
    /**
     * Разница в днях
     *
     * @param first
     * @param second
     * @returns {number}
     */
    _diffDays(first, second) {
        let date1     = new Date(first),
            date2     = new Date(second),
            time_diff = date2.getTime() - date1.getTime();
        
        return Math.ceil(time_diff / (1000 * 3600 * 24));
    }
    
    /**
     * Возвращает объект инстанса
     *
     * @param obj
     * @returns {*}
     */
    _with(obj) {
        return obj;
    }
    
    /**
     * Нормализцует event
     *
     * @param event_name
     * @returns {*}
     */
    _parseEventName(event_name) {
        let split = event_name.split(' '),
            event = `${event_name}.${this.constructor.name}`;
        
        if (split.length > 1) {
            event = [];
            
            $.each(split, (i, e) => {
                event.push(`${e}.${this.constructor.name}`);
            });
            
            event = event.join(' ');
        }
        
        return event;
    }
    
    /**
     * Копировать объект
     *
     * @param obj
     * @returns {*}
     */
    _copyObj(obj) {
        return Object.assign({}, obj);
    }
    
    /**
     * Колтчество элементов в объекте
     *
     * @param obj
     * @returns {number}
     */
    _objSize(obj) {
        let size = 0, key;
        
        for (key in obj) {
            if (obj.hasOwnProperty(key)) {
                size ++;
            }
        }
        
        return size;
    }
    
    /**
     * Это тач девайс?
     *
     * @returns {*}
     */
    _isTouchDevice() {
        let prefixes = ' -webkit- -moz- -o- -ms- '.split(' '),
            mq       = (query) => {
                return window.matchMedia(query).matches;
            }
        
        if (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) {
            return true;
        }
        
        var query = ['(', prefixes.join('touch-enabled),('), 'heartz', ')'].join('');
        
        return mq(query);
    }
    
    /**
     * Триггер ванильного события
     *
     * @param element Элемент
     * @param event_name Событие
     */
    _triggerVanillaEvent(element, event_name) {
        let event;
        
        if (typeof (Event) === 'function') {
            event = new Event(event_name);
        } else {
            event = document.createEvent('Event');
            event.initEvent(event_name, true, true);
        }
        
        element.dispatchEvent(event);
    }
    
    /**
     * Аналог php функции
     *
     * @param number
     * @param decimals
     * @param decPoint
     * @param thousandsSep
     *
     * @returns {string}
     */
    _number_format(number, decimals, decPoint, thousandsSep) {
        
        number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
        
        let n    = ! isFinite(+ number) ? 0 : + number,
            prec = ! isFinite(+ decimals) ? 0 : Math.abs(decimals),
            sep  = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep,
            dec  = (typeof decPoint === 'undefined') ? '.' : decPoint,
            s    = '';
        
        let toFixedFix = function (n, prec) {
            if (('' + n).indexOf('e') === - 1) {
                return + (Math.round(n + 'e+' + prec) + 'e-' + prec);
            } else {
                var arr = ('' + n).split('e'),
                    sig = '';
                
                if (+ arr[1] + prec > 0) {
                    sig = '+';
                }
                
                return (+ (Math.round(+ arr[0] + 'e' + sig + (+ arr[1] + prec)) + 'e-' + prec)).toFixed(prec);
            }
        }
        
        s = (prec ? toFixedFix(n, prec).toString() : '' + Math.round(n)).split('.');
        
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        
        if ((s[1] || '').length < prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1).join('0');
        }
        
        return s.join(dec);
    }
    
    /**
     * Обновляет get параметр
     *
     * @param string key
     * @param string value
     *
     * @returns {string}
     */
    _updateQueryStringParameter(key, value) {
        let uri       = window.location.href,
            re        = new RegExp('([?&])' + key + '=.*?(&|$)', 'i'),
            separator = uri.indexOf('?') !== - 1 ? '&' : '?';
        
        return uri.match(re) ? uri.replace(re, `$1${key}=${value}$2`) : `${uri}${separator}${key}=${value}`;
    }
}