<?php

$disable_cache = false;

if (isset($_GET['disable_cache'])) {
    $disable_cache = true;
}

/**
 * Выключить кэширование
 */
define('CACHE_DISABLED', $disable_cache);

$root_dir = dirname(__DIR__, 1);

/**
 * Включена модерация?
 */
define("MODERATE", false);

/**
 * Корневая директория
 */
define('ROOT_DIR', $root_dir);

/**
 * Директория шаблонов
 */
define('TPL_DIR', $root_dir . '/_resources/tpl/');

/**
 * Папка конфигов
 */
define('CONFIG_DIR', $root_dir . '/_resources/config/');

/**
 * Папка модулей
 */
define('MODULES_DIR', $root_dir . '/_resources/modules/');

/**
 * Папка расширений
 */
define('EXTENSIONS_DIR', $root_dir . '/_resources/extensions/');

/**
 * Автозагрузка классов
 */
require_once ROOT_DIR . '/vendor/autoload.php';

/**
 * Подключение расширений
 */
foreach (get_extensions() as $extension) {
    $files = array_diff(scandir($extension), ['..', '.']);
    if (in_array('include.php', array_diff(scandir($extension), ['..', '.']))) {
        require_once($extension . '/include.php');
    }
}

/**
 * Роуты
 */
require_once ROOT_DIR . '/_resources/routes.php';

/**
 * Обработка роутов
 */
$route = $_SERVER['REQUEST_URI'];
$route = preg_replace('~\?(.+?)$~isu', '', $route);

if (($route !== '/')) {
    $route = ltrim($route, '/');
    
    if (isset($routes[$route])) {
        $action = $routes[$route];
        $data   = explode('@', $action);
        $class  = $data[0];
        $method = $data[1];
        
        if (file_exists(ROOT_DIR . '/_resources/http/' . $class . '.php')) {
            $class_name = '\App\\' . $class;
            $instance   = new $class_name();
            
            if (method_exists($instance, $method)) {
                $instance->$method();
            }
        }
        
    } else {
        $app = new \App\Basic();
        $app->notFound();
    }
} else {
    $app = new \App\Basic();
    $app->index();
}

exit();
